

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var optionName: UILabel!
    @IBOutlet weak var optionImage: UIImageView!
    func setCell(title : String) {
        
        optionName.text = title
        
    }
}

