//
//  DatesCell.swift
//  hello
//
//  Created by Macbook on 10/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class TimesCell: UITableViewCell {
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var timeImage: UIImageView!
    
    func setCell(timeModel : TimesModel) {
        // print(dateModel.dateNumber)
        
        time.text = timeModel.time
    }
    
}
