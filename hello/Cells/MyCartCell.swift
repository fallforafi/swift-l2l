//
//  MyCartCell.swift
//  hello
//
//  Created by Macbook on 16/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class MyCartCell: UITableViewCell {

    @IBOutlet weak var serviceQuantity: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    
    @IBOutlet weak var price: UILabel!
    
    
    func setCell(id: Int,title: String,servicePrice: Double,quantity: Int,image: String){
        
        name.text = title
        price.text = String(servicePrice)
    
    serviceQuantity.text = String(quantity)
    
    }

}
