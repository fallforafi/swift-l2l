//
//  DiscountCodesCell.swift
//  hello
//
//  Created by Macbook on 26/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class DiscountCodesCell: UITableViewCell {
    
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var worthValue: UILabel!
    
    func setCell(discountActiveModel : DiscountCodesModel) {
        
        code.text = discountActiveModel.code
        worthValue.text = discountActiveModel.worth
        
    }
    
    @IBAction func copyCode(_ sender: Any) {
        UIPasteboard.general.string = worthValue.text
    }
}
