//
//  SettingsCellTableViewCell.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//
import UIKit

class LoyaltyCodesCell: UITableViewCell {
    
    @IBOutlet weak var code: UILabel!
    
    
    @IBOutlet weak var worthValue: UILabel!
    
    func setCell(loyaltyActiveModel : LoyaltyActiveModel) {
        
        code.text = loyaltyActiveModel.code
        worthValue.text = loyaltyActiveModel.worth
        //print(loyaltyActiveModel.worth)
    }
    
    @IBAction func copyCode(_ sender: Any) {
        UIPasteboard.general.string = worthValue.text
        //let vc = UIViewController()
        //vc.showMessage(title: "Error", msg: "sss")
        
    }
}
