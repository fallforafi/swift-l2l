//
//  SettingsCellTableViewCell.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//
import UIKit

class DiscountsHistoryCell: UITableViewCell {
    
    
    @IBOutlet weak var invoice: UILabel!
    
    @IBOutlet weak var worth: UILabel!
    
    @IBOutlet weak var createdDate: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    func setCell(discountHistoryModel : DiscountHistoryModel) {
        
        worth.text = discountHistoryModel.worth
        invoice.text = discountHistoryModel.invoice
        createdDate.text = "Date: " + discountHistoryModel.created
        amount.text = discountHistoryModel.amount
        
    }
    
    
}

