//
//  InvoicesCell.swift
//  hello
//
//  Created by Macbook on 27/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class InvoicesCell: UITableViewCell {
    
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var invoice: UILabel!
    @IBOutlet weak var pickupDate: UILabel!
    @IBOutlet weak var deliveryDate: UILabel!
    
    func setCell(invoicesModel : InvoicesModel) {
        amount.text = invoicesModel.amount
        invoice.text = invoicesModel.invoice
        pickupDate.text = invoicesModel.pickupDate
        deliveryDate.text = invoicesModel.deliveryDate
    }
    
}
