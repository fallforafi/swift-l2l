//
//  SettingsCellTableViewCell.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//
import UIKit
import iOSDropDown


class SettingsCell: UITableViewCell {
    
    @IBOutlet weak var dropDown: DropDown!
    @IBOutlet weak var category: UILabel!
    
    var pickOption = ["one", "two", "three", "seven", "fifteen"]
    
    func setSettings(setting : SettingsModel) {
        
        category.text = setting.category
        dropDown.selectedIndex=1;
        dropDown.optionArray = ["Option 1", "Option 2", "Option 3"]
        dropDown.optionIds = [1,23,54]
        dropDown.didSelect{(selectedText , index ,id) in
            self.category.text = "Selected String: \(selectedText) \n index: \(index)"
        }
        //dropDown.becomeFirstResponder()
        dropDown.addTarget(self, action: #selector(myTargetFunction), for: UIControl.Event.touchDown)
dropDown.resignFirstResponder()
        
        dropDown.isEnabled = true
        dropDown.isUserInteractionEnabled = true
        
        
    }
    
    func setTitle(){
    category.text = "ssss"
    }
    
    @objc func myTargetFunction(textField: DropDown) {
        print("myTargetFunction")
    }
}
