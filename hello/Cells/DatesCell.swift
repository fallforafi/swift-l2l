//
//  DatesCell.swift
//  hello
//
//  Created by Macbook on 10/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class DatesCell: UITableViewCell {

    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var dateImage: UIImageView!
    func setCell(dateModel : DatesModel) {
        print(dateModel.dateNumber)
        
        date.text = dateModel.dateList
    }

}
