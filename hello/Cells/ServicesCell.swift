//
//  ServicesCell.swift
//  hello
//
//  Created by Macbook on 01/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import SQLite

class ServicesCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!

    @IBOutlet weak var bg: UIImageView!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var discountPercentage: UILabel!
    @IBOutlet weak var quantity: UILabel!
    var service: ServicesModel!
    var  db = Cart(version: 1)
    
    func setCell(service: ServicesModel) {
        self.service = service
        
        
        
        let size:CGFloat = 35
        quantity.textColor = UIColor.black
        quantity.textAlignment = .center
        quantity.font = UIFont.systemFont(ofSize: 14.0)
        quantity.bounds = CGRect(x : 0.0,y : 0.0,width : size, height :  size)
        quantity.layer.cornerRadius = size / 2
        quantity.layer.borderWidth = 0
        //countLabel.layer.masksToBounds = true
        quantity.layer.backgroundColor = UIColor.white.cgColor
        quantity.layer.borderColor = UIColor.white.cgColor
        
        quantity.center = CGPoint(x:25.0,y: 178.0)
        quantity.translatesAutoresizingMaskIntoConstraints = false
        
        
        let items = db.getService(service:service,invoice_id:0)
        
        
        
        //side_nav_bar_bg
        bg.backgroundColor = UIColor(patternImage: UIImage(named: "side_nav_bar_bg")!)

        
        discountPercentage.isHidden = true
        db.createTable(version: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func minus(_ sender: Any) {
       
        var serviceQuantity = 0
        let count = db.existCart(service: service,invoice_id:0)
        
        
        if(count>0){
            serviceQuantity = db.getServiceQuantity(service: service,invoice_id:0)
            serviceQuantity = serviceQuantity-1
            db.updateCart(service: service,invoice_id:0,quantity: serviceQuantity)
        
            if(serviceQuantity==0){
                db.deleteCart(service: service,invoice_id:0)
            }
        
        }
        quantity.text = String(serviceQuantity)
    }
    
    
    @IBAction func add(_ sender: Any) {
        
        
        let count = db.existCart(service: service,invoice_id:0)
        var serviceQuantity = db.getServiceQuantity(service: service,invoice_id:0)
        
        if(count == 0){
            serviceQuantity = 1
           db.insertCart(service: service,invoice_id:0)
        }else{
            serviceQuantity = serviceQuantity + 1
            db.updateCart(service: service,invoice_id:0,quantity: serviceQuantity)
        }
        
        quantity.text = String(serviceQuantity)
    }
}


extension String {
    /*
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
    */
}
