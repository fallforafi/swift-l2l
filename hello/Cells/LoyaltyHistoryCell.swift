//
//  SettingsCellTableViewCell.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//
import UIKit

class LoyaltyHistoryCell: UITableViewCell {
    
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var invoice: UILabel!
    @IBOutlet weak var createdDate: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    func setCell(loyaltyHistoryModel : LoyaltyHistoryModel) {
        
        points.text = loyaltyHistoryModel.points
        invoice.text = loyaltyHistoryModel.invoice
        createdDate.text = "Date: " + loyaltyHistoryModel.created
        amount.text = loyaltyHistoryModel.amount
    }
    

}
