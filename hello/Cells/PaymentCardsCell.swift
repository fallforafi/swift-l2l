//
//  SettingsCellTableViewCell.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//
import UIKit

class PaymentCardsCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var expiry: UILabel!
    var id = ""
    func setCell(paymentCard : PaymentCardsModel) {
        
        name.text = paymentCard.name
        title.text = paymentCard.title
        number.text = paymentCard.number
        expiry.text = paymentCard.month+"/"+paymentCard.year
        
        self.id = paymentCard.id
    }
    
    
    
    @IBAction func addNewCard(_ sender: Any) {
        print(id)
    }
    
    @IBAction func editCard(_ sender: Any) {

    }
}
