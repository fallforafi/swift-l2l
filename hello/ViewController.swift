//
//  ViewController.swift
//  hello
//
//  Created by Macbook on 28/02/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON
class ViewController: BaseController {

    let preferences = UserDefaults.standard
    let uuid = NSUUID().uuidString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


    @IBAction func ukAction(_ sender: UIButton) {
        
        country = "uk"
        preferences.set(country, forKey: "country")
        registerDevice(country: country)
        
    }
    
    @IBAction func uaeAction(_ sender: UIButton) {
        country = "uae"
        preferences.set("country", forKey: country)
        registerDevice(country: country)
    }
    
    
    func registerDevice(country: String){

        let c=Countries(country:country)
        guard let url = URL(string: c.getRegisterDeviceURL()) else {
          
            return
        }
            Alamofire.request(url,
                          method: .post,
                          parameters: ["id":uuid,"model": UIDevice.current.model,"version": UIDevice.current.systemVersion,"platform": "ios"])
            .validate()
            
            .responseSwiftyJSON{ response in
                
                //print(response.data)   // result of response serialization
                
                
                let json = JSON(response.data as Any)
                
                device_id=json["device_detail"]["PKDeviceID"].stringValue
                
                self.preferences.set(device_id, forKey: "device_id")
                
                self.preferences.synchronize()
                //self.performSegue(withIdentifier: country, sender: Any?.self)
                
        }
        
    }
}
