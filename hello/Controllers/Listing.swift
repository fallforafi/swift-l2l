//
//  Listing.swift
//  hello
//
//  Created by Macbook on 29/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON
import SwipeableTabBarController
class Listing: SwipeableTabBarController {
    
    let preferences = UserDefaults.standard
    var tapGesture = UITapGestureRecognizer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        
        
        
        let code = UserDefaults.standard.string(forKey: "post_code") ?? ""
        let franchise_id = UserDefaults.standard.string(forKey: "franchise_id") ?? ""
        // Create Tab one
        
        var categories : [Services] = []
 swipeAnimatedTransitioning?.animationType = SwipeAnimationType.push
        isSwipeEnabled = true

        country = UserDefaults.standard.string(forKey: "country") ?? "uk"
        device_id = UserDefaults.standard.string(forKey: "device_id") ?? ""
        
        
        
        let c=Countries(country:country)
        guard let url = URL(string: c.getAreaURL()) else {
            
            return
        }
        
        Alamofire.request(url,
                          method: .post,
                          parameters: ["type":"load",
                                       "device_id": device_id,
                                       "post_code": code as Any,
                                       "versions": "1|2|3",
                                       "more_info": "1"])
            .validate()
            
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                
                if(json["categories"]["data"].count>0){
                    
                
                    var i = 1
                    //var services : [[ServicesModel]] = []
                    var services : [ServicesModel] = []
                    
                    for category in json["categories"]["data"]
                    {
                        
                        let tab = Services()
                        let categoryTitle = category.1["MTitle"].stringValue
                        let serviceRecords = category.1["service_records"]
                        let mobileIcon = category.1["MobileIcon"].stringValue
                        
                        
                        for data in serviceRecords
                        {
                            let record=data.1
                            
                            let categoy_id = record["CategoryID"].stringValue
                           
                            let serviceModel=ServicesModel(desktopImageName: record["DesktopImageName"].stringValue,desktopImage: record["DesktopImage"].stringValue,desktopImagePath: record["DesktopImagePath"].stringValue,preferencesShow: record["PreferencesShow"].stringValue,discountPercentage: record["DiscountPercentage"].stringValue,service_id: record["PKServiceID"].stringValue,package: record["Package"].stringValue,offerPrice: record["OfferPrice"].stringValue,isPackage: record["IsPackage"].stringValue,id: record["ID"].stringValue,currencyAmount: record["CurrencyAmount"].stringValue,titleUpper: record["TitleUpper"].stringValue,title: record["Title"].stringValue,content: record["Content"].stringValue,price: record["Price"].stringValue,mobileImageName: record["MobileImageName"].stringValue,mobileImagePath: record["MobileImagePath"].stringValue,mobileImage: record["MobileImage"].stringValue,category_id: categoy_id,category: categoryTitle,country: country,device_id: device_id)
                            
                            
                                services.append(serviceModel)
                            
                        }
                        
                        
                        tab.services = services
                        
                        
                        var img = UIImage(named: mobileIcon)
                        //img?.size.width = 2
                        let tabOneBarItem = UITabBarItem(title: categoryTitle , image: img, selectedImage: img)
                        
                        tab.tabBarItem = tabOneBarItem
                        categories.append(tab)
                        i = i + 1
                    }
                
                self.viewControllers = categories
                }else{
                    
                }
        }
        
        
        updateCheckoutBar()

        
    }
    
    func updateCheckoutBar(){
        
        let checkoutBarView = CheckoutBar.instanceFromNib()
        
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        self.tapGesture.numberOfTapsRequired = 1
        self.tapGesture.numberOfTouchesRequired = 1
        checkoutBarView.addGestureRecognizer(self.tapGesture)
        checkoutBarView.isUserInteractionEnabled = true
        self.view.addSubview(checkoutBarView)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "Checkout") as! Checkout
        self.navigationController?.pushViewController(VC1, animated: true)
        self.present(VC1, animated: true)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }
}
