//
//  ServicesPopup.swift
//  hello
//
//  Created by Macbook on 15/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON
import Kingfisher

class ServicesPopup: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    let cellReuseIdentifier = "ServicesCell"
    var services : [ServicesModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        tableView.allowsSelection = false
        
        tableView.backgroundColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let servicesCell = UINib(nibName: cellReuseIdentifier,bundle: nil)
        
        tableView.register(servicesCell, forCellReuseIdentifier: cellReuseIdentifier)
        
        
        
        let code = UserDefaults.standard.string(forKey: "post_code") ?? "e12pe"
        let franchise_id = UserDefaults.standard.string(forKey: "franchise_id") ?? "1"
        
        var categories : [Services] = []
        
        country = UserDefaults.standard.string(forKey: "country") ?? "uk"
        device_id = UserDefaults.standard.string(forKey: "device_id") ?? "123"
        
        let c=Countries(country:country)
        
        guard let url = URL(string: c.getAreaURL()) else {
            
            return
        }
        
        
        Alamofire.request(url,
                          method: .post,
                          parameters: ["type":"load",
                                       "device_id": device_id,
                                       "post_code": code as Any,
                                       "versions": "1|2|3",
                                       "more_info": "1"])
            .validate()
            
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                
                if(json["categories"]["data"].count>0){
                    
                    
                    var i = 1
                    
                    
                    for category in json["categories"]["data"]
                    {
                        let serviceRecords = category.1["service_records"]
                        print(serviceRecords.count)
                        
                        let categoryTitle = category.1["MTitle"].stringValue
                        
                        let mobileIcon = category.1["MobileIcon"].stringValue
                        
                        
                        for data in serviceRecords
                        {
                            let record=data.1
                            
                            let categoy_id = record["CategoryID"].stringValue
                            
                            let serviceModel=ServicesModel(desktopImageName: record["DesktopImageName"].stringValue,desktopImage: record["DesktopImage"].stringValue,desktopImagePath: record["DesktopImagePath"].stringValue,preferencesShow: record["PreferencesShow"].stringValue,discountPercentage: record["DiscountPercentage"].stringValue,service_id: record["PKServiceID"].stringValue,package: record["Package"].stringValue,offerPrice: record["OfferPrice"].stringValue,isPackage: record["IsPackage"].stringValue,id: record["ID"].stringValue,currencyAmount: record["CurrencyAmount"].stringValue,titleUpper: record["TitleUpper"].stringValue,title: record["Title"].stringValue,content: record["Content"].stringValue,price: record["Price"].stringValue,mobileImageName: record["MobileImageName"].stringValue,mobileImagePath: record["MobileImagePath"].stringValue,mobileImage: record["MobileImage"].stringValue,category_id: categoy_id,category: categoryTitle,country: country,device_id: device_id)
                            
                            
                            self.services.append(serviceModel)
                            
                        }
                        
                        
                    
                    }
                   
                }else{
                    
                }
        self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for:indexPath) as! ServicesCell
        let service = self.services[indexPath.row]
        cell.title.text = service.title
        
        
        
        cell.setCell(service: service)
        
        
        let url = URL(string: service.mobileImagePath)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: cell.bounds.size.height-2))
        imageView.kf.setImage(with: url)
        
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.white.cgColor
        cell.frame = CGRect(x: 0,y: 0,width: self.tableView.frame.size.width,height: cell.frame.size.height)
        //cell.bounds.size.width = UIScreen.main.bounds.size.width
        //imageView.kf_setImageWithURL(url)
        cell.backgroundView = UIView()
        cell.backgroundView!.addSubview(imageView)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 200.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, widthForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;//Choose your custom row height
    }
}
