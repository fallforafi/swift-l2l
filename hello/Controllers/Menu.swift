//
//  MenuController.swift
//  hello
//
//  Created by Macbook on 07/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class Menu: UIViewController {
    
    var menuOptions = ["Login","Register","Dashboard","Area","Settings","Invoices"]
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
}


extension Menu: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        cell.setCell(title: menuOptions[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let option = menuOptions[indexPath.row]
        performSegue(withIdentifier: option, sender: option)
    }
}
