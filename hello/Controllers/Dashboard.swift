//
//  Login.swift
//  hello
//
//  Created by Macbook on 11/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class Dashboard: BaseController {
    
    let preferences = UserDefaults.standard
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
                let c=Countries(country:country)
        guard let url = URL(string: c.getSettingsURL() + "/" + String(member_id)) else { return }
        
                //print(url)
                Alamofire.request(url,
                                  parameters: [device_id:device_id])
                    .validate()
                    .responseSwiftyJSON{ response in
                        
                        let json = JSON(response.data as Any)
                        let message=json["message"].stringValue
                        let result=json["result"].stringValue
                        //print(json)
                        
                        if(result=="Success"){
                            
                        }else{
                            
                            self.showMessage(title: "Error", msg: message,style: .alert)
                            
                        }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.async {
                
                
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let nav = segue.destination as! UINavigationController
        let form = nav.topViewController as! Invoices
        form.type = segue.identifier!
        
    }
    
    
    
    
    @IBAction func pending(_ sender: Any) {
        
        performSegue(withIdentifier: "pending", sender: Any.self)
    }
    
    
    @IBAction func processed(_ sender: Any) {
        performSegue(withIdentifier: "processed", sender: Any.self)
    }
    
    @IBAction func cancelled(_ sender: Any) {
        performSegue(withIdentifier: "cancelled", sender: Any.self)
    }
    
    @IBAction func completed(_ sender: Any) {
        performSegue(withIdentifier: "completed", sender: Any.self)
    }
    

}
