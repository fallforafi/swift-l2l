//
//  Settings.swift
//  hello
//
//  Created by Macbook on 18/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON


class PaymentCards: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    let preferences = UserDefaults.standard
    var paymentCards : [PaymentCardsModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c=Countries(country:country)
        
        let url = c.getPaymentCardsURL()+"/"+member_id
        tableView.allowsSelection = false
        
        Alamofire.request(url,
                          parameters: [device_id:device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                let cards=json["cards"]
                if(cards.count>0){
                    
                    for card in cards{
                        
                        self.paymentCards.append(PaymentCardsModel(id: card.1["ID"].stringValue,title: card.1["Title"].stringValue,name: card.1["Name"].stringValue,number: card.1["MaskedNumber"].stringValue,month: card.1["ExpireMonth"].stringValue,year: card.1["ExpireYear"].stringValue,cvv: card.1["MaskedCode"].stringValue))
                    }
                    self.tableView.reloadData()


                }
        }
        
    }
    
    @IBAction func addCard(_ sender: Any) {
        
        let pc = PaymentCardsModel.self
        performSegue(withIdentifier: "segue", sender: pc)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let nav = segue.destination as! UINavigationController
        let form = nav.topViewController as! PaymentCardForm
        
        
        if segue.identifier == "segue" {
            form.model = (sender as! PaymentCardsModel)
            form.type = "edit"
        }
        else
        {
            form.type = "create"
        }
        
        
    }
    
    
}

extension PaymentCards: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let paymentCard = paymentCards[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardsCell") as! PaymentCardsCell
        cell.setCell(paymentCard: paymentCard)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let paymentCard = paymentCards[indexPath.row]
        performSegue(withIdentifier: "segue", sender: paymentCard)
    }
}
