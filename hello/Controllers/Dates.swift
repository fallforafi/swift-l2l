//
//  Dates.swift
//  hello
//
//  Created by Macbook on 09/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class Dates: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var type : String = ""
    var dates : [DatesModel] = []
    
    var onSave : (( _ type: String, _ dateNumber: String,_ dateList: String) -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()
        let franchise_id = UserDefaults.standard.string(forKey: "franchise_id") ?? ""
        
    
        let c=Countries(country:country)
        guard let url = URL(string: c.getPickupDateURL() + "/" + franchise_id) else {
            
            return
        }
        
        Alamofire.request(url,method: .post,parameters: ["device_id": device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                
                if(json["pick"].count>0){
                    let datesJson=json["pick"]
                    if(datesJson.count>0){
                        
                        for date in datesJson{
                            
                            self.dates.append(DatesModel(dateClass: date.1["Date_Selected"].stringValue, dateNumber: date.1["Date_Number"].stringValue, dateList: date.1["Date_List"].stringValue, dateSelected: date.1["Date_Selected"].stringValue))
                        }
                        
                        //print(self.dates)
                        self.tableView.reloadData()
                    }
                }else{
                    
                }
        }
        
    }

    @IBAction func close(_ sender: Any) {
        
        dismiss(animated: true)    }
}



extension Dates: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let date = dates[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DatesCell") as! DatesCell
        cell.setCell(dateModel: date)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let date = dates[indexPath.row]
        onSave?(type,date.dateNumber,date.dateList)
        dismiss(animated: true)
    }
}
