//
//  Dates.swift
//  hello
//
//  Created by Macbook on 09/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class Times: UIViewController {
    
    var type : String = ""
    var pickupDate : String = ""
    var pickupTime : String = ""
    var deliveryDate : String = ""
    var times : [TimesModel] = []
    var onTimeClick : (( _ type: String, _ time: String,_ hour: String) -> ())?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let franchise_id = UserDefaults.standard.string(forKey: "franchise_id") ?? ""
        let country = UserDefaults.standard.string(forKey: "country") ?? ""
        let c=Countries(country: country)
        let request = c.getPickupTimeURL() + "/?id=" + franchise_id + "&date=2019-04-10&pick_date=2019-04-10&device_id=123&type=Pickup"
        
        
        
        guard let url = URL(string: request) else {
            return
        }
        
        Alamofire.request(url,method: .get,parameters: [:])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                
                if(json["data"].count>0){
                    let timesJson=json["data"]
                    if(timesJson.count>0){
                        
                        for time in timesJson{
                             self.times.append(TimesModel(status: time.1["Class"].stringValue, time: time.1["Time"].stringValue, hour: time.1["Hour"].stringValue))
                        }
                        self.tableView.reloadData()
                    }
                }else{
                    
                }
        }
        
    }
    

}

extension Times: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return times.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let time = times[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimesCell") as! TimesCell
        cell.setCell(timeModel: time)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let time = times[indexPath.row]
        onTimeClick?(type,time.time,time.hour)
        dismiss(animated: true)
    }
}

