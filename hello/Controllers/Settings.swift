//
//  Settings.swift
//  hello
//
//  Created by Macbook on 18/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON


class Settings: BaseController {
    @IBOutlet weak var tableView: UITableView!
    let preferences = UserDefaults.standard
    var settings : [SettingsModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.allowsSelection = false
        tableView.isEditing = true

        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c=Countries(country:country)
        
        let url = c.getSettingsURL()+"?member_id="+member_id
        
        self.settings.append(SettingsModel(id:"123" ,title:"asdd",category_id: "1",category:"sadad"))
        print(url)
        
        Alamofire.request(url,
                          parameters: [device_id:device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                //let message=json["message"].stringValue
                //let result=json["result"].stringValue
                let preferences=json["preferences"]
                let memberPreferences = preferences["member_preferences"]
                
                
                let data: NSMutableDictionary = NSMutableDictionary()

                for p in json["preferences"]["data"] {
                    
                    data.setValue(p.1["Title"].stringValue, forKey: p.1["ID"].stringValue)

                }
                var t = data["1"] as! String
                var category_id = ""
                var id = ""
                var title = ""
                for m in memberPreferences {
                    
                    category_id=m.0
                    title = m.1["Title"].stringValue
                    id=m.1["ID"].stringValue
                    t = data[category_id] as! String
                    self.settings.append(SettingsModel(id:id ,title:title,category_id: category_id,category:t))
                }
                self.tableView.reloadData()
                
        }
        
    }
}


extension Settings: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let setting = settings[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
        
        cell.setSettings(setting: setting)
        return cell
    }
}
