//
//  DiscountsHistory.swift
//  hello
//
//  Created by Macbook on 26/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class DiscountsHistory: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    let preferences = UserDefaults.standard
    var discounts : [DiscountHistoryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c = Countries(country:country)
        
        let url = c.getDiscountCodesURL()+"/"+member_id
        tableView.allowsSelection = false;
        
        Alamofire.request(url,
                          parameters: [device_id:device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                let discountHistory=json["data"]["discount_history"]
                
                
                if(discountHistory.count>0){
                    
                    for code in discountHistory{
                        self.discounts.append(DiscountHistoryModel(id: code.1["ID"].stringValue,invoice:code.1["InvoiceNumber"].stringValue,code:code.1["DiscountCode"].stringValue,amount:code.1["CurrencyAmount"].stringValue,created:code.1["CreatedDateTimeFormatted"].stringValue,worth:code.1["DiscountWorthValue"].stringValue))
                        
                        
                    }
                    self.tableView.reloadData()
                    
                    
                }
        }
        
    }
}

extension DiscountsHistory: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return discounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let discount = discounts[indexPath.row]
        print(discount.amount)
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountsHistoryCell") as! DiscountsHistoryCell
        
        cell.setCell(discountHistoryModel: discount)
        return cell
    }
    
}
