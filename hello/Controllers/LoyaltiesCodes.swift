//
//  Settings.swift
//  hello
//
//  Created by Macbook on 18/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON


class LoyaltiesCodes: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    let preferences = UserDefaults.standard
    var loyaltyPoints : [LoyaltyActiveModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c=Countries(country:country)
        
        let url = c.getLoyaltyCodesURL()+"/"+member_id
        tableView.allowsSelection = false;
        print(url)
        
        Alamofire.request(url,
                          parameters: [device_id:device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                let loyaltyActive=json["data"]["loyalty_active"]
                if(loyaltyActive.count>0){
                    
                    for code in loyaltyActive{
                        
                        self.loyaltyPoints.append(LoyaltyActiveModel(id: code.1["ID"].stringValue,code:code.1["Code"].stringValue,worth:code.1["WorthValue"].stringValue))
                    
                    
                    }
                    self.tableView.reloadData()
                    
                    
                }
        }
        
    }
}

extension LoyaltiesCodes: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loyaltyPoints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let loyalty = loyaltyPoints[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoyaltyCodesCell") as! LoyaltyCodesCell
        
        print(loyalty)
        cell.setCell(loyaltyActiveModel: loyalty)
        return cell
    }
    
}
