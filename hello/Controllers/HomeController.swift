//
//  ViewController.swift
//  hello
//
//  Created by Macbook on 28/02/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class HomeController: BaseController {
    
    let preferences = UserDefaults.standard
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func ukAction(_ sender: Any) {
        
        
        country = "uk"
        UserDefaults.standard.set(country, forKey: "country")
        registerDevice(country: country)
        
        
    }
    
    @IBAction func uaeAction(_ sender: Any) {
        
        country = "uae"
        UserDefaults.standard.set(country, forKey: "country")
        registerDevice(country: country)
    }
    
    func registerDevice(country: String){
        
        let c=Countries(country:country)
        guard let url = URL(string: c.getRegisterDeviceURL()) else {
            
            return
        }
        
        
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        
        
        Alamofire.request(url,
                          method: .post,
                          parameters: ["id":uuid,"model": UIDevice.current.model,"version": "","platform": "ios"])
            .validate()
            
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                
                device_id=json["device_detail"]["PKDeviceID"].stringValue
                
                UserDefaults.standard.set(device_id, forKey: "device_id")
                
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "Area") as! Area
                self.navigationController!.pushViewController(VC1, animated: true)
                
        }
    }
}
