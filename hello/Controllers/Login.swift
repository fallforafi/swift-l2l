//
//  Login.swift
//  hello
//
//  Created by Macbook on 11/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class Login: BaseController {
    
    let preferences = UserDefaults.standard
    var activityIndicator = UIActivityIndicatorView(style: .gray)

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var iconTextField: UITextField! {
        didSet {
            iconTextField.tintColor = UIColor.lightGray
            let ic_email = UIImage(named: "icon_email")
            iconTextField.setIcon(image: ic_email!)
            
        }
    }
    
    @IBOutlet weak var iconPasswordField: UITextField! {
        didSet {
            iconPasswordField.tintColor = UIColor.lightGray
            let ic_password = UIImage(named: "ic_password")
            iconPasswordField.setIcon(image: ic_password!)
            
        }
    }
    
    @IBOutlet weak var forgotButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconTextField.layer.cornerRadius = 10; // this value vary as per your desire
        iconTextField.clipsToBounds = true;
        iconTextField.layer.borderWidth=0;
        
    }
    
    @IBAction func login(_ sender: Any) {
        
    
        let email: String = iconTextField.text!

        let password: String = iconPasswordField.text!
        
        if email == "" {
            
            showAlertMessage(titleStr: "Error",messageStr: "Please enter email address.")
            
        }else if password == "" {
            
            showAlertMessage(titleStr: "Error",messageStr: "Please enter your password.")
            
        }else{
            
          //  let sv = UIViewController.displaySpinner(onView: self.view)
            
            let c=Countries(country:country)
            guard let url = URL(string: c.getLoginURL()) else { return }
            
            Alamofire.request(url,
                              method: .post,
                              parameters: ["email_address":email ,"password": password,"device_id": 123])
                .validate()
                
                .responseSwiftyJSON{ response in
                    
                    let json = JSON(response.data as Any)
                    let member_id=json["member"]["ID"].stringValue
                    
                    print(json)
                    
                    let firstName = json["member"]["FirstName"].stringValue
                    let lastName = json["member"]["LastName"].stringValue
                    let phone = json["member"]["Phone"].stringValue
                    let postCode = json["member"]["PostalCode"].stringValue
                    let buildingName = json["member"]["BuildingName"].stringValue
                    let streetName = json["member"]["StreetName"].stringValue
                    let town = json["member"]["Town"].stringValue
                    
                    
                    
                    self.preferences.set(member_id, forKey: "member_id")
                    
              
                    self.preferences.set(firstName, forKey: "firstName")
                     self.preferences.set(lastName, forKey: "lastName")
                     self.preferences.set(phone, forKey: "phone")
                     self.preferences.set(buildingName, forKey: "buildingName")
                     self.preferences.set(streetName, forKey: "streetName")
                     self.preferences.set(town, forKey: "town")
                     self.preferences.set(email, forKey: "email")
                     self.preferences.set(postCode, forKey: "postCode")
 
                    
                    
                    self.preferences.synchronize()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "Dashboard")
                    self.present(controller, animated: true, completion: nil)
                    
            }
            
            
            
            
            
        }
    }
    
    func showAlertMessage(titleStr:String, messageStr:String) -> Void {
        
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true)
    }

}


extension UITextField {
    func setIcon(image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 0, y: 10, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 30, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
    
}
