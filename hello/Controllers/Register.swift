//
//  Login.swift
//  hello
//
//  Created by Macbook on 11/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON
import TextFieldEffects
class Register: BaseController {
    
    let preferences = UserDefaults.standard
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    
    @IBOutlet weak var firstNameField: AkiraTextField!
    
    @IBOutlet weak var lastNameField: AkiraTextField!
    
    
    @IBOutlet weak var phoneField: AkiraTextField!
    
    @IBOutlet weak var postCodeField: AkiraTextField!
    
    @IBOutlet weak var buildingField: AkiraTextField!
    
    @IBOutlet weak var streetField: AkiraTextField!
    
    @IBOutlet weak var townField: AkiraTextField!
    
    @IBOutlet weak var emailField: AkiraTextField!
    
    @IBOutlet weak var passwordField: AkiraTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        
        var ic = UIImage(named: "icon_email")
        firstNameField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        lastNameField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        phoneField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        postCodeField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        buildingField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        streetField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        townField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        emailField.setIcon(image: ic!)
        
        ic = UIImage(named: "icon_email")
        passwordField.setIcon(image: ic!)
        
        
    }
    
    @IBAction func register(_ sender: Any) {
        
        
        let firstName: String = firstNameField.text!
        let lastName: String = lastNameField.text!
        let phone: String = phoneField.text!
        let postCode: String = postCodeField.text!
        let building: String = buildingField.text!
        let street: String = streetField.text!
        let town: String = townField.text!
        let email: String = emailField.text!
        let password: String = passwordField.text!
        
        
        if firstName == "" {
            
            showAlertMessage(titleStr: "Error",messageStr: "Please enter first name.")
            
        }else if lastName == "" {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter last name.")
        
        }else if phone == "" {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter phone number.")
            
        }else if postCode == "" {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter post code.")
            
        }else if building == "" {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter building name or number.")
            
        }else if street == "" {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter street name.")
            
        }else if town == "" {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter town name.")
            
        }else if email == "" {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter email address.")
            
        }else if !isValidEmail(testStr: email) {
            showAlertMessage(titleStr: "Error",messageStr: "Please enter corect email address.")
            
        }else{
            
            //  let sv = UIViewController.displaySpinner(onView: self.view)
            
            let c=Countries(country:country)
            guard let url = URL(string: c.getRegiserURL()) else { return }
            
            print(url)
            Alamofire.request(url,
                              method: .post,
                              parameters: ["email_address":email ,
                                           "password": password,
                                           "first_name":firstName,
                                           "last_name":lastName,
                                           "building_name":building,
                                           "street_name":street,
                                           "town":town,
                                           "phone":phone,
                                           "post_code":postCode,
                                           "device_id": 123])
                .validate()
                
                .responseSwiftyJSON{ response in
                    
                    let json = JSON(response.data as Any)
                    let message=json["message"].stringValue
                    let result=json["result"].stringValue
                    
                    if(result=="Success"){
                        
                        let member_id=json["MemberID"].stringValue
                        self.preferences.set(member_id, forKey: "member_id")
                        self.preferences.synchronize()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "Dashboard")
                        self.present(controller, animated: true, completion: nil)
                    }else{
                     
                        self.showAlertMessage(titleStr: "Error", messageStr: message)
                        
                    }
            }
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showAlertMessage(titleStr:String, messageStr:String) -> Void {
        
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
}
