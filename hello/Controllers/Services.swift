import Foundation
import UIKit
import Kingfisher
//import AlamofireImage


class Services: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    var tableView: UITableView = UITableView()
    let cellReuseIdentifier = "ServicesCell"
    var services : [ServicesModel] = []
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.frame = CGRect(x: 0, y: 80, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height-250)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        
        tableView.backgroundColor = .white
        
        let servicesCell = UINib(nibName: cellReuseIdentifier,bundle: nil)
        
        self.tableView.register(servicesCell, forCellReuseIdentifier: cellReuseIdentifier)
        
        self.view.addSubview(tableView)
 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for:indexPath) as! ServicesCell
        let service = services[indexPath.row]
        cell.title.text = service.title
        cell.setCell(service: service)
        
        
        let url = URL(string: service.mobileImagePath)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: cell.bounds.size.height-2))
        imageView.kf.setImage(with: url)

        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.white.cgColor
        cell.frame = CGRect(x: 0,y: 0,width: self.tableView.frame.size.width,height: cell.frame.size.height)
        //cell.bounds.size.width = UIScreen.main.bounds.size.width
        //imageView.kf_setImageWithURL(url)
        cell.backgroundView = UIView()
        cell.backgroundView!.addSubview(imageView)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 200.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, widthForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;//Choose your custom row height
    }
    
}
