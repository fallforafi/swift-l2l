//
//  InvoiceDetailViewController.swift
//  hello
//
//  Created by Macbook on 27/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class InvoiceDetail: BaseController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var data : InvoicesModel?
    //var type = String()
    
    let preferences = UserDefaults.standard
    
    @IBOutlet weak var invoice: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var total: UILabel!
    
    @IBOutlet weak var created: UILabel!
    @IBOutlet weak var pickup: UILabel!
    @IBOutlet weak var delivery: UILabel!
    
    
    @IBOutlet weak var address2: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var id = 11524
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c = Countries(country:country)
        
        let url = c.getInvoiceURL()
            //+ "?id= " + id + "&member_id=" + member_id + "&device_id=123"
        print(url)
        
        var parameters: Parameters = ["device_id": device_id as Any, "member_id": member_id as Any, "id": id as Any]

        
        Alamofire.request(url,
                          parameters: parameters)
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                let result = json["result"]
                print(json["data"])
                
                if result.stringValue == "Success" {
                    
                    let data = json["data"]
                    
                    
                    self.address.text = data["BuildingName"].stringValue + " " + data["StreetName"].stringValue
                    + " " + data["Town"].stringValue
                    
                    self.total.text = data["CurrencyAmount"].stringValue
                    self.status.text = "Status: " + data["OrderStatus"].stringValue

                    self.created.text = "Created Time: " + data["CreatedDateTimeFormatted"].stringValue
                    
                    self.pickup.text = "Pickup Time: " + data["PickUpDateTimeFormatted"].stringValue
                    
                    self.delivery.text = "Delivery Time: " + data["DeliveryDateTimeFormatted"].stringValue
                    
                }else{
                    
                }
                
                
                
                
        }
        
        
    }
    @IBAction func cancel(_ sender: Any) {
        
        
    }
}
