//
//  PaymentCardForm.swift
//  hello
//
//  Created by Macbook on 21/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class PaymentCardForm: BaseController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let preferences = UserDefaults.standard
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var expiryField: UITextField!
    @IBOutlet weak var cvvField: UITextField!
    var model : PaymentCardsModel?
    var monthInteger = 0
    
    
    
    var pickOption = [["January","Febuary", "March", "April", "May", "June","July","August","September","Octuber","November","December"],[]]

    @IBOutlet weak var save: UIButton!
    
    var id = String()
    var type = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(model as Any)
        //print(model?.title as Any)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        expiryField.inputView = pickerView
        
        var year = Calendar.current.component(.year, from: Date()) - 1
        var  i = 0
        var j = ""
        while i <= 15 {
            year = year + 1
            j=String(year)
            pickOption[1].append(j)
            i = i + 1
        }
        
        nameField.text = model?.name
        titleField.text = model?.title
        numberField.text = model?.number
        cvvField.text = model?.cvv


        var s = 2
        if type == "edit" {
            s = Int((model?.month)!)!
           
            s = s - 1
            expiryField.text = (pickOption[0][s]) + "/" + (model?.year)!
            monthInteger = s
        }else{
            expiryField.text = (pickOption[0][1]) + "/" + (pickOption[1][1])
            monthInteger = 0
        }
        
        
    }
    
    @IBAction func saveCard(_ sender: Any) {
        
        
        let name   : String = nameField.text!
        let title  : String = titleField.text!
        let number : String = numberField.text!
        let expiry : String = expiryField.text!
        let cvv : String = cvvField.text!
        
        
        var arr = expiry.split{$0 == "/"}.map(String.init)

        let month = monthInteger + 1
        //let year = arr[1].suffix(2)
        let year = arr[1]
        
        if name == "" {
            
            showMessage(title: "Error", msg: "Please enter name.")
            
        }else if title == "" {
        
             showMessage(title: "Error", msg: "Please enter title.")
        
        }else if number == "" && number.count==16 {
            
            showMessage(title: "Error", msg: "Invalid credit card number.")
            
        }else if cvv == "" {
            
            showMessage(title: "Error", msg: "Invalid cvv code.")
            
        }else{
            
            //  let sv = UIViewController.displaySpinner(onView: self.view)
            
            let c=Countries(country:country)
            guard let url = URL(string: c.getPostPaymentCardsURL()) else { return }
            
            print(url)
            
            let member_id = preferences.string(forKey: "member_id")!
            //var device_id = preferences.string(forKey: "device_id")!
            let device_id = 123
            var parameters: Parameters = ["title": title as Any, "name": name as Any, "number": number as Any,"member_id":  member_id,"code":cvv , "month":month, "year" : year,"device_id": device_id]

            
            if(type == "create"){
                parameters["id"]=""
            }else if(type == "edit"){
                parameters["id"]=model?.id
            }else{
                 parameters["id"]=model?.id
                 type = "remove"
            }
            parameters["type"]=type
            
            print(parameters)
            
            Alamofire.request(url,
                              method: .post,
                              parameters: parameters )
                .validate()
                .responseSwiftyJSON{ response in
                    
                    let json = JSON(response.data as Any)
                    let message=json["message"].stringValue
                    let result=json["result"].stringValue
                    
                    if(result=="Success"){
                        
                        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "PaymentCards") as! PaymentCards
                        self.navigationController!.pushViewController(VC1, animated: true)
                        
                        
                    }else{
                        
                        self.showMessage(title: "Error", msg: message,style: .alert)
                        
                    }
            }
        }
    }
    
    @IBAction func deleteCard(_ sender: Any) {
    
        self.type="remove"
        saveCard((Any).self)
    
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption[component].count
    }
    
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let m = pickOption[0][pickerView.selectedRow(inComponent: 0)]
        let y = pickOption[1][pickerView.selectedRow(inComponent: 1)]
        monthInteger = pickerView.selectedRow(inComponent: 0)
        //expiryField.text = m + "/" + y
    }
    
}
