//
//  UKController.swift
//  hello
//
//  Created by Macbook on 06/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON


var franchise = ""

class Area: BaseController {
    
    let preferences = UserDefaults.standard

    @IBOutlet weak var areaTitle: UILabel!
    @IBOutlet weak var areaDescription: UILabel!
    @IBOutlet weak var postCode: UITextField!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var checkPrice: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    

    @IBAction func listing(_ sender: Any) {
        
        
        
        let c=Countries(country:country)
        
        guard let url = URL(string: c.getAreaURL()) else {
            
            return
        }
        
        let code = postCode.text
        print(code)
        device_id = UserDefaults.standard.string(forKey: "device_id") ?? ""
        
        Alamofire.request(url,
                          method: .get,
                          parameters: ["type":"fetch",
                                       "device_id": device_id,
                                       "post_code": code!,
                                       "versions": "1|2|3",
                                       "more_info": "1"])
            .validate()
            
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                print(json)
                if(json["categories"]["data"].count>0){
                
                    UserDefaults.standard.set(code!, forKey: "post_code")
                
                    UserDefaults.standard.set(json["franchise_detail"]["ID"].stringValue, forKey: "franchise_id")
                    
                UserDefaults.standard.set(json["franchise_detail"]["MinimumOrderAmount"].stringValue, forKey: "minimum_order_amount")
                    
                UserDefaults.standard.set(json["franchise_detail"]["DeliveryOption"].stringValue, forKey: "delivery_option")
                    
                     let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "Listing") as! Listing
                     self.navigationController?.pushViewController(VC1, animated: true)
                    self.present(VC1, animated: true)
                    
                
                }else{
                    
                }
                
                
                
        }
    }
}
