//
//  BaseController.swift
//  hello
//
//  Created by Macbook on 29/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

var device_id = ""
var country = ""

class BaseController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let image = UIImage(named: "ic_menu_white_3x")?.withRenderingMode(.alwaysOriginal)

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target:self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        //self.view.addGestureRecognizer((self.revealViewController()?.panGestureRecognizer())!)
        //self.revealViewController()?.rearViewRevealWidth = 240
        
    }
}

extension UIViewController {
    
    
    
    func displaySpinner() -> Spinner{
        
        let spinner = Spinner()
        addChild(spinner)
        spinner.view.frame = view.frame
        view.addSubview(spinner.view)
        
        spinner.didMove(toParent: self)
        return spinner;
    }
    
    func removeSpinner(spinner : Spinner) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            // then remove the spinner view controller
            spinner.willMove(toParent: nil)
            spinner.view.removeFromSuperview()
            spinner.removeFromParent()
        }
    }
    
    func showMessage(title : String?, msg : String,
                     style: UIAlertController.Style = .alert,
                     dontRemindKey : String? = nil) {
        if dontRemindKey != nil,
            UserDefaults.standard.bool(forKey: dontRemindKey!) == true {
            return
        }
        
        let ac = UIAlertController.init(title: title,
                                        message: msg, preferredStyle: style)
        ac.addAction(UIAlertAction.init(title: "OK",
                                        style: .default, handler: nil))
        
        if dontRemindKey != nil {
            ac.addAction(UIAlertAction.init(title: "Don't Remind",
                                            style: .default, handler: { (aa) in
                                                UserDefaults.standard.set(true, forKey: dontRemindKey!)
                                                UserDefaults.standard.synchronize()
            }))
        }
        DispatchQueue.main.async {
            self.present(ac, animated: true, completion: nil)
        }
    }
    
    
    
}


