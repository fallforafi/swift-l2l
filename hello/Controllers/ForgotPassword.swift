//
//  ForgotPassword.swift
//  hello
//
//  Created by Macbook on 15/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON
import TextFieldEffects
class ForgotPassword: BaseController {
    
    let preferences = UserDefaults.standard

    @IBOutlet weak var emailField: KaedeTextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        var ic = UIImage(named: "icon_email")
        emailField.setIcon(image: ic!)
    }
    
    @IBAction func forgot(_ sender: Any) {
        
        
        let email: String = emailField.text!
        
        if email == "" {
            showMessage(title: "Error", msg: "Please enter email address.", style: .alert)
        // UserHasSeenMsg
        }else{
            
            let c=Countries(country:country)
            guard let url = URL(string: c.getForgotPasswordURL()) else { return }
            var spinner=self.displaySpinner()
            Alamofire.request(url,
                              method: .post,
                              parameters: ["email_address":email ,
                                           "device_id": 123])
                .validate()
                
                .responseSwiftyJSON{ response in
                    
                    let json = JSON(response.data as Any)
                    let message=json["message"].stringValue
                    let result=json["result"].stringValue
                    
                    if(result=="Success"){
                      /*
                        let member_id=json["MemberID"].stringValue
                        self.preferences.set(member_id, forKey: "member_id")
                        self.preferences.synchronize()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "Dashboard")
                        self.present(controller, animated: true, completion: nil)
                    */
                    }else{
                       self.showMessage(title: "Error", msg: message, style: .alert)
                    
                    }
                    self.removeSpinner(spinner : spinner)
            }
            
            
            
            
            
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidLoad()
    }
    
    
    
}
