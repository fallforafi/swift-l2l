//
//  LoyaltiesHistoryViewController.swift
//  hello
//
//  Created by Macbook on 26/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class LoyaltiesHistory: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    let preferences = UserDefaults.standard
    var loyaltyPoints : [LoyaltyHistoryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c = Countries(country:country)
        
        let url = c.getLoyaltyCodesURL()+"/"+member_id
        tableView.allowsSelection = false;
        
        Alamofire.request(url,
                          parameters: [device_id:device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                let loyaltyHistory=json["data"]["loyalty_history"]
                if(loyaltyHistory.count>0){
                    
                    for code in loyaltyHistory{
                        
                        self.loyaltyPoints.append(LoyaltyHistoryModel(id: code.1["ID"].stringValue,invoice:code.1["InvoiceNumber"].stringValue,points:code.1["Points"].stringValue,amount:code.1["CurrencyAmount"].stringValue,created:code.1["CreatedDateTimeFormatted"].stringValue))
                        
                     
                    }
                    self.tableView.reloadData()
                    
                    
                }
        }
        
    }
}

extension LoyaltiesHistory: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loyaltyPoints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let loyalty = loyaltyPoints[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoyaltyHistoryCell") as! LoyaltyHistoryCell
        
        print(loyalty)
        cell.setCell(loyaltyHistoryModel: loyalty)
        return cell
    }
    
}
