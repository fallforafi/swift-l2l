//
//  Account.swift
//  hello
//
//  Created by Macbook on 15/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON
import TextFieldEffects

class Account: BaseController, UITextFieldDelegate{
    let preferences = UserDefaults.standard
    
    
    @IBOutlet weak var firstNameField: AkiraTextField!
    
    @IBOutlet weak var lastNameField: AkiraTextField!
    
    @IBOutlet weak var phoneField: AkiraTextField!
    
    @IBOutlet weak var postCodeField: AkiraTextField!
    
    @IBOutlet weak var buildingField: AkiraTextField!
    
    @IBOutlet weak var streetField: AkiraTextField!
    
    
    @IBOutlet weak var townField: AkiraTextField!
    
    @IBOutlet weak var emailField: AkiraTextField!
    
    
    @IBOutlet weak var passwordField: AkiraTextField!
    
    @IBOutlet weak var confirmPasswordField: AkiraTextField!
    
    var member_id : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
member_id = preferences.string(forKey: "member_id")!
        
    
    }
    
    override func viewDidAppear(_ animated: Bool) {

        DispatchQueue.global(qos: .userInitiated).async {
            
            // Bounce back to the main thread to update the UI
            DispatchQueue.main.async {
           
            }
        }
    }
    
    @IBAction func updateAccount(_ sender: Any) {
        
        
        let firstName: String = firstNameField.text!
        let lastName: String = lastNameField.text!

        let phone: String = phoneField.text!
        let postCode: String = postCodeField.text!
        let building: String = buildingField.text!
        let street: String = streetField.text!
        let town: String = townField.text!
        let email: String = emailField.text!
        let password: String = passwordField.text!
        
        
        if firstName == "" {
            
            showMessage(title: "Error",msg: "Please enter first name.", style: .alert)
            
        }else if lastName == "" {
            showMessage(title: "Error",msg: "Please enter last name.", style: .alert)
            
        }else if phone == "" {
            showMessage(title: "Error",msg: "Please enter phone number.", style: .alert)
            
            
        }else if postCode == "" {
            showMessage(title: "Error",msg: "Please enter post code.", style: .alert)
            
        }else if building == "" {
            showMessage(title: "Error",msg: "Please enter building name or number.", style: .alert)
            
        }else if street == "" {
            showMessage(title: "Error",msg: "Please enter street name.", style: .alert)
            
        }else if town == "" {
            showMessage(title: "Error",msg: "Please enter town name.", style: .alert)
            
        }else if email == "" {
            showMessage(title: "Error",msg: "Please enter email address.", style: .alert)
            
        }else if password != confirmPasswordField.text {
            showMessage(title: "Error",msg: "Both password did not match.", style: .alert)
            
        }else{
            
            let c=Countries(country:country)
            guard let url = URL(string: c.getAccountUpdateURL()) else { return }
            
            print(url)
            Alamofire.request(url,
                              method: .post,
                              parameters: ["id":member_id,
                                           "email_address":email ,
                                          "password": password,
                                          "first_name":firstName,
                                           "last_name":lastName,
                                           "building_name":building,
                                           "street_name":street,
                                           "town":town,
                                           "phone":phone,
                                           "post_code":postCode,
                                           "device_id": 123])
                .validate()
                
                .responseSwiftyJSON{ response in
                    
                    let json = JSON(response.data as Any)
                    let message=json["message"].stringValue
                    let result=json["result"].stringValue
    
                    
                    if(result=="Success"){
                        
                        self.preferences.set(firstName, forKey: "firstName")
                        self.preferences.set(lastName, forKey: "lastName")
                        self.preferences.set(phone, forKey: "phone")
                        self.preferences.set(building, forKey: "buildingName")
                        self.preferences.set(street, forKey: "streetName")
                        self.preferences.set(town, forKey: "town")
                        self.preferences.set(email, forKey: "email")
                        self.preferences.set(postCode, forKey: "postCode")
                        
                        self.preferences.synchronize()
                        
                        self.showMessage(title: "Success", msg: message,style: .alert)
                        
                    }else{
                        
                        self.showMessage(title: "Error", msg: message,style: .alert)
                        
                    }
            }
        }
    }
    
}
