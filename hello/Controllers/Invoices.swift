//
//  Invoices.swift
//  hello
//
//  Created by Macbook on 27/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class Invoices: BaseController {

    var type = String()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    let preferences = UserDefaults.standard
    var invoices : [InvoicesModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c = Countries(country:country)
        
        let url = c.getInvoicesURL()+"/"+member_id+"?type="+type
        //tableView.allowsSelection = false;
        
        
        Alamofire.request(url,
                          parameters: [device_id:device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                let invoicesJson=json["data"]
                
                
                if(invoicesJson.count>0){
                    
                    for code in invoicesJson{
                        
                        self.invoices.append(InvoicesModel(id: code.1["ID"].stringValue,invoice: code.1["InvoiceNumber"].stringValue, amount: code.1["CurrencyAmount"].stringValue,status: code.1["OrderStatus"].stringValue,pickupDate: code.1["PickupDateTimeFormatted"].stringValue,deliveryDate: code.1["DeliveryDateTimeFormatted"].stringValue ))
                        
                        
                    }
                    self.tableView.reloadData()
                }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let nav = segue.destination as! UINavigationController
        //let form = nav.topViewController as! InvoiceDetail
        
    }
   
}

extension Invoices: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return invoices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let invoice = invoices[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoicesCell") as! InvoicesCell
        
        cell.setCell(invoicesModel: invoice)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let invoice = invoices[indexPath.row]
        
        performSegue(withIdentifier: "invoice_detail", sender: indexPath.row)
    }
    
    
}
