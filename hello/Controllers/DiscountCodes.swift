//
//  DiscountsCodes.swift
//  hello
//
//  Created by Macbook on 26/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit
import UIKit
import Alamofire
import SwiftyJSON
import Alamofire_SwiftyJSON

class DiscountCodes: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    let preferences = UserDefaults.standard
    var discountCodes : [DiscountCodesModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let member_id : String = preferences.string(forKey: "member_id")!
        
        let c = Countries(country:country)
        
        let url = c.getDiscountCodesURL()+"/"+member_id
        tableView.allowsSelection = false;
        print(url)
        
        Alamofire.request(url,
                          parameters: [device_id:device_id])
            .validate()
            .responseSwiftyJSON{ response in
                
                let json = JSON(response.data as Any)
                let discountActive=json["data"]["discount_active"]
                if(discountActive.count>0){
                    
                    for code in discountActive{
                        
                        self.discountCodes.append(DiscountCodesModel(id: code.1["ID"].stringValue,code:code.1["Code"].stringValue,worth:code.1["WorthValue"].stringValue))
                        
                        
                    }
                    self.tableView.reloadData()
                    
                    
                }
        }
        
    }
}

extension DiscountCodes: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discountCodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let discount = discountCodes[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountCodesCell") as! DiscountCodesCell
cell.setCell(discountActiveModel: discount)
        return cell
    }
    
}
