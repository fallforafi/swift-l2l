import UIKit
import McPicker

class Checkout: UIViewController {

    @IBOutlet weak var pickupDateField: UIButton!
    
    @IBOutlet weak var pickupTimeField: UIButton!
    
    @IBOutlet weak var deliveryDateField: UIButton!
    
    @IBOutlet weak var deliveryTimeField: UIButton!
    
    @IBOutlet weak var myCart: UITableView!
    
    @IBOutlet weak var myPreferences: UITableView!
    
    
    var pickupDate : String = ""
    var pickupTime : String = ""
    var pickupTimeHour : String = ""
    
    @IBOutlet weak var referralButton: UIButton!
    @IBOutlet weak var discountButton: UIButton!
    @IBOutlet weak var codeField: UITextField!
    @IBOutlet weak var applyButton: UIButton!
    
    var service: ServicesModel!
    var  db = Cart(version: 1)
    var services = [ServicesModel]()
    var cart = [CartModel]()
    var showDiscountArea : DarwinBoolean = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myCart.alwaysBounceVertical = false

        myCart.isScrollEnabled = false

        
        updateMyCart()
        
        applyButton.isHidden = true
        codeField.isHidden = true
        

    }
    @IBAction func discountButtonAction(_ sender: Any) {
        
        codeField.placeholder="Discount Code"
        toogleDiscount()
    }
    
    @IBAction func referralButtonAction(_ sender: Any) {
        codeField.placeholder="Referral Code"
        toogleDiscount()
    }
    
    @IBAction func applyAction(_ sender: Any) {
    
    }
    
    func toogleDiscount(){
        
        applyButton.isHidden = false
        codeField.isHidden = false
    }
    
    func updateMyCart(){
         self.cart = db.getCart(invoice_id: 0)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        scrollView.contentOffset.x = 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let type : String = segue.identifier!
        
        if(segue.identifier == "pickupdate" || segue.identifier == "deliverydate" )
        {
            let popup = segue.destination as! Dates
            popup.type = type
            popup.onSave = onSave
            
        }else if(segue.identifier == "time"){
            let popup = segue.destination as! Times
            popup.type = type
            popup.pickupDate = pickupDate
            popup.onTimeClick = onTimeClick
        }
    }
    
    
    func onSave( _ type: String, _ dateNumber: String,_ dateList: String) -> (){
        pickupDateField.titleLabel?.text = dateList
        
        if(type == "pickupdate"){
            pickupDate = dateNumber
        }
        
    }
    
    func onTimeClick( _ type: String, _ time: String,_ hour: String) -> (){
        pickupTimeField.titleLabel?.text = time
        pickupTimeHour = hour
    }
    
}


extension Checkout: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        var count : Int = 0
        
        if tableView == self.myCart {
            count = self.cart.count
        }
        
        if tableView == self.myPreferences {
            count = 3
        }
        
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell2:UITableViewCell?
        
        
            let service = cart[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCartCell") as! MyCartCell
            
            cell.setCell(id: service.service_id,title: service.title,servicePrice: service.price,quantity: service.quantity,image: service.mobileImagePath)
            return cell
        
    }
    
}

