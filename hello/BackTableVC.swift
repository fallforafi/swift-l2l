//
//  BackTableVC.swift
//  hello
//
//  Created by Macbook on 11/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation


class BackTableVC: UITableViewController {
 
    var TableArray = [String]();
    
    override func viewDidLoad() {
        
        TableArray=["Login","Register","Dashboard","Account"]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TableArray[indexPath.row],for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = TableArray[indexPath.row]
        
        return cell
        
    }
}
