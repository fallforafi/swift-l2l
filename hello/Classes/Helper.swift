//
//  Helper.swift
//  hello
//
//  Created by Macbook on 05/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

public class Helper {



    
    static func  displayErrorMessage(title: String,message : String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
       // self.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        
        let alert = UIAlertController(title: "Did you bring your towel?", message: "It's recommended you bring your towel before continuing.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        vc.present(alert, animated: true)
    }


}
