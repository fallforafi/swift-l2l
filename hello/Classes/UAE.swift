//
//  UAE.swift
//  hello
//
//  Created by Macbook on 05/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation
public class UAE {
    
     public static let COUNTRY_CODE = "uae"
     public static let CURRENCY_CODE = "AED"
     public static let CURRENCY_SYMBOL = "AED"
     public static let CURRENCY = "dirham"
     public static let SERVER = "http://www.graspdeal.com/love2laundyuae"
     public static let API_LOAD_DATA = "/apiv5/get_load_data"
     public static let API_LOGIN = "/apiv5/post_login"
     public static let API_REGISTER = "/apiv5/post_register"
     public static let API_CHECKOUT = "/apiv5/post_invoice_checkout"
     public static let API_PICK_UP_DATE = "/apiv5/get_pickup_date_records"
     public static let API_PICK_UP_TIME = "/apiv5/get_time_records"
     public static let API_DELIVERY_DATE = "/apiv5/get_delivery_date_records"
     public static let API_DELIVERY_TIME = "/apiv5/get_time_records"
     public static let API_CREDIT_CARDS = "/apiv5/get_credit_cards"
     public static let API_REGISTER_DEVICE = "/apiv5/post_register_device"
     public static let API_PREFERENCES = "/apiv5/get_preferences_records"
    
     public static let API_POST_PREFERENCES = "/apiv5/post_preferences_update"
    
     public static let API_INVOICE = "/apiv5/get_invoice_detail"
     public static let API_DASHBOARD = "/apiv5/dashboard"
     public static let API_INVOICES = "/apiv5/get_invoices"
     public static let API_EDIT_INVOICE = "/apiv5/post_invoice_edit"
     public static let API_CANCEL_INVOICE = "/apiv5/post_invoice_re_order_copy"
    
     public static let API_LOYALTIES = "/apiv5/get_loyalty"
     public static let API_DISCOUNTS = "/apiv5/get_discounts"
    
     public static let API_POST_DISCOUNT = "/apiv5/post_discount_code"
     public static let API_POST_REFERRAL = "/apiv5/post_referral_code"
     public static let API_FORGOT_PASSWORD = "/apiv5/post_forgot_password"
    
}
