//
//  Countries.swift
//  hello
//
//  Created by Macbook on 05/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

public class Countries {
    
    var country: String = "uk"
    init(country: String ){
        self.country = country
        
        
    }
    
    func getCountry() -> String{
        return self.country;
    }
    
     func getRegisterDeviceURL() -> String{
    
        if(self.country=="uk"){
            return UK.SERVER + UK.API_REGISTER_DEVICE
        }else{
            return UAE.SERVER + UAE.API_REGISTER_DEVICE
        }
        
    }
    
    func getAreaURL() -> String{
        
        if(self.country=="uk"){
            return UK.SERVER + UK.API_LOAD_DATA
        }else{
            return UAE.SERVER + UAE.API_LOAD_DATA
        }
        
    }
    
    
    func getPickupDateURL() -> String{
        
        if(self.country=="uk"){
            return UK.SERVER + UK.API_PICK_UP_DATE
        }else{
            return UAE.SERVER + UAE.API_PICK_UP_DATE
        }
        
    }
    
    func getPickupTimeURL() -> String{
        
        if(self.country=="uk"){
            return UK.SERVER + UK.API_PICK_UP_TIME
        }else{
            return UAE.SERVER + UAE.API_PICK_UP_TIME
        }
        
    }
    
    
    
    func getLoginURL() -> String{
        return UK.SERVER + UK.API_LOGIN

        /*
        if(self.country=="uk"){
            return UK.SERVER + UK.API_LOGIN
        }else{
            return UAE.SERVER + UAE.API_LOGIN
        }
         */
        
    }
    
    func getRegiserURL() -> String{
        return UK.SERVER + UK.API_REGISTER
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + UK.API_LOGIN
         }else{
         return UAE.SERVER + UAE.API_LOGIN
         }
         */
        
    }
    
    
    
    func getDashboardURL() -> String{
        return UK.SERVER + UK.API_DASHBOARD
        /*
         if(self.country=="uk"){
         return UK.SERVER + UK.API_LOGIN
         }else{
         return UAE.SERVER + UAE.API_LOGIN
         }
         */
    }
    
    func getSettingsURL() -> String{
        return UK.SERVER + UK.API_PREFERENCES
        /*
         if(self.country=="uk"){
         return UK.SERVER + UK.API_LOGIN
         }else{
         return UAE.SERVER + UAE.API_LOGIN
         }
         */
    }
    
    func getAccountUpdateURL() -> String{
        return UK.SERVER + UK.API_ACCOUNT_UPDATE
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + UK.API_LOGIN
         }else{
         return UAE.SERVER + UAE.API_LOGIN
         }
         */
        
    }
    
    func getForgotPasswordURL() -> String{
        return UK.SERVER + UK.API_FORGOT_PASSWORD
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + UK.API_LOGIN
         }else{
         return UAE.SERVER + UAE.API_LOGIN
         }
         */
        
    }
    
    func getLoyaltyCodesURL() -> String{
        return UK.SERVER + UK.API_LOYALTIES
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + API_LOYALTIES
         }else{
         return UAE.SERVER + API_LOYALTIES
         }
         */
        
    }
    
    func getInvoicesURL() -> String{
        return UK.SERVER + UK.API_INVOICES
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + API_LOYALTIES
         }else{
         return UAE.SERVER + API_LOYALTIES
         }
         */
        
    }
    
    func getInvoiceURL() -> String{
        return UK.SERVER + UK.API_INVOICE
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + API_LOYALTIES
         }else{
         return UAE.SERVER + API_LOYALTIES
         }
         */
        
    }
    
    
    func getDiscountCodesURL() -> String{
        return UK.SERVER + UK.API_DISCOUNTS
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + API_LOYALTIES
         }else{
         return UAE.SERVER + API_LOYALTIES
         }
         */
        
    }
    
    func getPaymentCardsURL() -> String{
        return UK.SERVER + UK.API_CREDIT_CARDS
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + UK.API_LOGIN
         }else{
         return UAE.SERVER + UAE.API_LOGIN
         }
         */
        
    }
    
    func getPostPaymentCardsURL() -> String{
        return UK.SERVER + UK.API_POST_CREDIT_CARD
        
        /*
         if(self.country=="uk"){
         return UK.SERVER + UK.API_LOGIN
         }else{
         return UAE.SERVER + UAE.API_LOGIN
         }
         */
        
    }
    
}
