//
//  Protocols.swift
//  hello
//
//  Created by Macbook on 07/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

protocol HomeControllerDelegate {
    func handleMenuToggle()
}
