//
//  DatesModel.swift
//  hello
//
//  Created by Macbook on 09/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation


class DatesModel{
    
    var dateClass: String
    var dateNumber: String
    var dateList: String
    var dateSelected: String
    
    
    init(dateClass: String,dateNumber: String,dateList: String,dateSelected: String) {
        self.dateClass = dateClass
        self.dateNumber = dateNumber
        self.dateList = dateList
        self.dateSelected = dateSelected
    }
}
