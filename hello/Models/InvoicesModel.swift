import Foundation
import UIKit

class InvoicesModel{
    
    var id: String
    var invoice: String
    var status: String
    var deliveryDate: String
    var pickupDate: String
    var amount: String
    
    init(id: String,invoice: String,amount: String,status: String,pickupDate: String,deliveryDate: String) {
        self.id = id
        self.invoice = invoice
        self.amount = amount
        self.status = status
        self.pickupDate = pickupDate
        self.deliveryDate = deliveryDate
    }
    
}
