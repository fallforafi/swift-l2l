//
//  Settings.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation
import UIKit

class PaymentCardsModel{
    
    var id: String
    var title: String
    var name: String
    var number: String
    var month: String
    var year: String
    var cvv: String
    
    init(id: String,title: String,name: String,number: String,month: String,year: String,cvv: String) {
        self.id = id
        self.title = title
        self.name = name
        self.number = number
        self.month = month
        self.year = year
        self.cvv = cvv
    }
    
}
