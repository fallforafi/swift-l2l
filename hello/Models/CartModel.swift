//
//  ServicesModel.swift
//  hello
//
//  Created by Macbook on 02/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation

class CartModel{
    
    var id: Int
    var service_id: Int
    var title: String
    var preferencesShow: String
    var discount: Double
    var package: String
    var offerPrice: String
    var currencyAmount: String
    var price: Double
    var mobileImageName: String
    var mobileImagePath: String
    var mobileImage: String
    var category_id: String
    var category: String
    var country: String
    var quantity: Int
    var invoice_id: Int
    var device_id: String
    
    init(id: Int,
         service_id: Int,
         title: String,
         quantity: Int,
         preferencesShow: String,
         discount: Double,
         package: String,
         offerPrice: String,
         currencyAmount: String,
         price: Double,
         mobileImageName: String,
         mobileImagePath: String,
         mobileImage: String,
         category_id: String,
         category: String,
         country: String,
         device_id: String,
         invoice_id: Int) {
        
        self.id = id
        self.service_id = service_id
        self.title = title
        self.quantity = quantity
        self.preferencesShow = preferencesShow
        self.discount = discount
        self.package = package
        self.offerPrice = offerPrice
        self.currencyAmount = currencyAmount
        self.price = price
        self.mobileImageName = mobileImageName
        self.mobileImagePath = mobileImagePath
        self.mobileImage = mobileImage
        self.category_id = category_id
        self.category = category
        self.country = country
        self.device_id = device_id
        self.invoice_id = invoice_id
        
    }
}
