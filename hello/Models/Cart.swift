//
//  CartModel.swift
//  hello
//
//  Created by Macbook on 02/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation
import SQLite

class Cart{
    
    var database: Connection!
    
    
    var databaseFile = "love2laundry"
    var table = Table("cart")
    let id = Expression<Int>("id")
    
    var services = [CartModel]()
    
    var service_id = Expression<Int>("service_id")
    let title = Expression<String>("title")
    let titleUpper = Expression<String>("titleUpper")
    let content = Expression<String>("content")
    let unitPrice = Expression<Double>("unitPrice")
    let price = Expression<Double>("price")
    let offerPrice = Expression<String?>("offerPrice")
    let currencyAmount = Expression<String?>("currencyAmount")
    let discount = Expression<Double>("discount")
    let category_id = Expression<Int>("category_id")
    let category = Expression<String>("category")
    let preferencesShow = Expression<String>("preferencesShow")
    let package = Expression<String?>("package")
    let desktopImage = Expression<String?>("desktopImage")
    let desktopImageName = Expression<String?>("desktopImageName")
    let desktopImagePath = Expression<String?>("desktopImagePath")
    let mobileImage = Expression<String?>("mobileImage")
    let mobileImageName = Expression<String>("mobileImageName")
    let mobileImagePath = Expression<String>("mobileImagePath")
    
    let allowed = Expression<Int>("allowed")
    let invoice_id = Expression<Int>("invoice_id")
    let device_id = Expression<String>("device_id")
    let quantity = Expression<Int>("quantity")
    let country = Expression<String>("country")
    let createdAt = Expression<Date>("createdAt")
    let updatedAt = Expression<Date?>("updatedAt")
    
    
    init(version: Int) {
        
        do{
            let documentDirectory = try FileManager.default.url(for: .documentDirectory,
                                                                in: .userDomainMask,appropriateFor: nil, create: true)
        
            let fileUrl = documentDirectory.appendingPathComponent(databaseFile).appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        }catch{
            print(error)
        }
    }
    
    func createTable(version: Int)
    {
        
        
        let createTable = self.table.create { t in
            t.column(id, primaryKey: true)
            t.column(self.service_id)
            t.column(self.title)
            t.column(self.titleUpper)
            t.column(self.content)
            t.column(self.unitPrice)
            t.column(self.price)
            t.column(self.offerPrice)
            t.column(self.currencyAmount)
            t.column(self.discount)
            t.column(self.category_id)
            t.column(self.category)
            t.column(self.preferencesShow)
            t.column(self.package)
            t.column(self.desktopImage)
            t.column(self.desktopImageName)
            t.column(self.desktopImagePath)
            t.column(self.mobileImage)
            t.column(self.mobileImageName)
            t.column(self.mobileImagePath)
            t.column(self.allowed)
            t.column(self.invoice_id)
            t.column(self.device_id)
            t.column(self.quantity)
            t.column(self.country)
            t.column(self.createdAt)
            t.column(self.updatedAt)
            //var s = ServicesModel()
            
        }
        
        do{
            try self.database.run(self.table.drop(ifExists: true))
            //try self.database.run(self.table.drop(ifExists: true))
            try self.database.run(createTable)
        } catch {
            print(error)
        }
    }
    
    func getCart(invoice_id: Int) -> [CartModel]
    {
        
        do {
            let all = Array(try database.prepare(table))
            
            for item in all {
                /*
                print("service_id: \(item[self.service_id])")
                print("quantity: \(item[self.quantity])")
                print("title: \(item[self.title])")
                print("titleUpper: \(item[self.titleUpper])")
                print("content: \(item[self.content])")
                print("unitPrice: \(item[self.unitPrice])")
                print("price: \(item[self.price])")
                print("offerPrice: \(item[self.offerPrice])")
                print("currencyAmount: \(item[self.currencyAmount])")
                print("discount: \(item[self.discount])")
                print("preferencesShow: \(item[self.preferencesShow])")
                print("desktopImage: \(item[self.desktopImage])")
                print("desktopImageName: \(item[self.desktopImageName])")
                print("desktopImagePath: \(item[self.desktopImagePath])")
                print("mobileImage: \(item[self.mobileImage])")
                print("mobileImageName: \(item[self.mobileImageName])")
                print("mobileImagePath: \(item[self.mobileImagePath])")
                
                print("allowed: \(item[self.allowed])")
                print("invoice_id: \(item[self.invoice_id])")
                print("device_id: \(item[self.device_id])")
                
                print("country: \(item[self.country])")
                print("createdAt: \(item[self.createdAt])")
                print("updatedAt: \(item[self.updatedAt])")
                */
                
                self.services.append(CartModel(id: item[self.id],
                                               service_id: item[self.service_id],
                                               title: item[self.title],
                                               quantity: item[self.quantity],
                                               preferencesShow: item[self.preferencesShow],
                                               discount: item[self.discount],
                                               package: item[self.package]!,
                                               offerPrice: String(item[self.currencyAmount]!),
                                               currencyAmount: item[self.currencyAmount]!,
                                               price: item[self.price],
                                               mobileImageName: item[self.mobileImageName],
                                               mobileImagePath: item[self.mobileImagePath],
                                               mobileImage: item[self.mobileImage]!,
                                               category_id: String(item[self.category_id]),
                                               category: item[self.category],
                                               country: item[self.country],
                                               device_id: item[self.device_id],
                                               invoice_id: 0))
            }
            
            return self.services
        } catch {
            print("existCart failed: \(error)")
        }
        
        return []
        
    }
    
    func getService(service: ServicesModel,invoice_id: Int) -> Int
    {
        var quantity: Int
        
        do{
            let all = Array(try database.prepare(table.filter(self.service_id == Int(service.id)!)))
            
            if(all.count>0){
                for cart in all {
                    quantity =  cart[self.quantity]
                    return quantity
                }
            }
            // return quantity
        }catch{
            print("existCart failed: \(error)")
        }
        return 0
    }
    
    
    
    func getServiceQuantity(service: ServicesModel,invoice_id: Int) -> Int
    {
        var quantity: Int
        
        do{
            let all = Array(try database.prepare(table.filter(self.service_id == Int(service.id)!)))
            
            if(all.count>0){
                for cart in all {
                     quantity =  cart[self.quantity]
                    return quantity
                }
            }
            // return quantity
        }catch{
            print("existCart failed: \(error)")
        }
        return 0
    }
    
    func existCart(service: ServicesModel,invoice_id: Int) -> Int
    {
        do {
            
        let sql="SELECT count(*) FROM cart where service_id = \(service.id) and country = '\(service.country)' and device_id = \(service.device_id)"
        let count = try self.database.scalar(sql) as! Int64
            return Int(count)
        } catch {
            print("existCart failed: \(error)")
        }
        return 0
    }
    
    func updateCart(service: ServicesModel,invoice_id: Int,quantity: Int) -> Int
    {
        do {
            
            let sql="update cart set quantity=\(quantity),price= unitPrice * \(quantity) where service_id = \(service.id) and device_id = \(service.device_id) and country = '\(service.country)'"
            try self.database.run(sql)
            return 1
        } catch {
            print("updateCart failed: \(error)")
        }
        return 0
    }
    
    func deleteCart(service: ServicesModel,invoice_id: Int) -> Int
    {
        do {
            
            let sql="delete from cart where service_id = \(service.id) and device_id = \(service.device_id) and country = '\(service.country)'"
            try self.database.run(sql)
            return 1
        } catch {
            print("updateCart failed: \(error)")
        }
        return 0
    }
    
    
    func insertCart(service: ServicesModel,invoice_id: Int) -> Int
    {
        
        
        do {
            //try self.database.run(table.delete())

            
            let cardId = try self.database.run(self.table.insert(
                            title <- service.title,
                            service_id <- Int(service.id)!,
                            titleUpper <- service.titleUpper,
                            category_id <- Int(service.category_id)!,
                            content <- service.content,
                            price <- Double(service.price)!,
                            unitPrice <- Double(service.price)!,
                            currencyAmount <- service.currencyAmount,
                            discount <- Double(service.discountPercentage)!,
                            preferencesShow <- service.preferencesShow,
                            package <- service.isPackage,
                            category <- service.category,
                            desktopImage <- service.desktopImage,
                            desktopImageName <- service.desktopImageName,
                            mobileImagePath <- service.mobileImagePath,
                            mobileImage <- service.mobileImage,
                            mobileImageName <- service.mobileImageName,
                            mobileImagePath <- service.mobileImagePath,
                            allowed <- 0,
                            quantity <- 1,
                            self.invoice_id <- invoice_id,
                            self.device_id <- service.device_id,
                            self.country <- service.country,
                            createdAt <- Date()
                        ))
            print("inserted id: \(cardId)")
            return Int(cardId)
        } catch {
            print("insertion failed: \(error)")
        }
        return 0
    }
    
    
}
