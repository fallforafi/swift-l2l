//
//  Settings.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation
import UIKit

class LoyaltyActiveModel{
    
    var id: String
    var code: String
    var worth: String
    init(id: String,code: String,worth: String) {
        self.id = id
        self.code = code
        self.worth = worth
    }
    
}
