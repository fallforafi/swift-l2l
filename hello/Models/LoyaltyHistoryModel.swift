import Foundation
import UIKit

class LoyaltyHistoryModel{
    
    var id: String
    var invoice: String
    var points: String
    var amount: String
    var created: String
    
    
    init(id: String,invoice: String,points: String,amount: String,created: String) {
        self.id = id
        self.invoice = invoice
        self.points = points
        self.amount = amount
        self.created = created
        
    }
    
}
