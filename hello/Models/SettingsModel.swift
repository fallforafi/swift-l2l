//
//  Settings.swift
//  hello
//
//  Created by Macbook on 19/03/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation
import UIKit

class SettingsModel{
    
    var id: String
    var title: String
    var category: String
    var category_id: String
    
    init(id: String,title: String,category_id: String,category: String) {
    
        self.id = id
        self.title = title
        self.category_id = category_id
        self.category = category
    }
    
}
