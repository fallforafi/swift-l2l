//
//  ServicesModel.swift
//  hello
//
//  Created by Macbook on 02/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation

class ServicesModel{
    
    var desktopImageName: String
    var desktopImage: String
    var desktopImagePath: String
    var preferencesShow: String
    var discountPercentage: String
    var service_id: String
    var package: String
    var offerPrice: String
    var isPackage: String
    var id: String
    var currencyAmount: String
    var titleUpper: String
    var title: String
    var content: String
    var price: String
    var mobileImageName: String
    var mobileImagePath: String
    var mobileImage: String
    var category_id: String
    var category: String
    
    var device_id: String
    var country: String
    
    init(desktopImageName: String,
         desktopImage: String,
         desktopImagePath: String,
         preferencesShow: String,
         discountPercentage: String,
         service_id: String,
         package: String,
         offerPrice: String,
         isPackage: String,
         id: String,
         currencyAmount: String,
         titleUpper: String,
         title: String,content: String,price: String,mobileImageName: String,mobileImagePath: String,mobileImage: String,category_id: String,category: String,country: String,device_id: String) {
        
        self.desktopImageName = desktopImageName
        self.desktopImage = desktopImage
        self.desktopImagePath = desktopImagePath
        self.preferencesShow = preferencesShow
        self.discountPercentage = discountPercentage
        self.service_id = service_id
        self.package = package
        self.offerPrice = offerPrice
        self.isPackage = isPackage
        self.id = id
        self.currencyAmount = currencyAmount
        self.titleUpper = titleUpper
        self.title = title
        self.content = content
        self.price = price
        self.mobileImageName = mobileImageName
        self.mobileImagePath = mobileImagePath
        self.mobileImage = mobileImage
        self.category_id = category_id
        self.category = category
        
        self.device_id = device_id
        self.country = country
        
    }
}
