import Foundation
import UIKit

class DiscountHistoryModel{
    
    var id: String
    var invoice: String
    var code: String
    var amount: String
    var created: String
    var worth: String
    
    init(id: String,invoice: String,code: String,amount: String,created: String,worth: String) {
        self.id = id
        self.invoice = invoice
        self.code = code
        self.amount = amount
        self.created = created
        self.worth = worth
    }
    
}
