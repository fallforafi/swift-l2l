import Foundation
import UIKit

class DiscountCodesModel{
    
    var id: String
    var code: String
    var worth: String
    
    
    init(id: String,code: String,worth: String) {
        self.id = id
        self.code = code
        self.worth = worth
    }
}
