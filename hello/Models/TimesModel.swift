//
//  DatesModel.swift
//  hello
//
//  Created by Macbook on 09/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import Foundation


class TimesModel{
    
    var status: String
    var time: String
    var hour: String
    
    init(status: String,time: String,hour: String) {
        self.status = status
        self.time = time
        self.hour = hour
    }
}
