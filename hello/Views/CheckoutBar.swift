//
//  checkoutBar.swift
//  hello
//
//  Created by Macbook on 05/04/2019.
//  Copyright © 2019 Macbook. All rights reserved.
//

import UIKit

class CheckoutBar: UIView {

    @IBOutlet weak var subTotal: UILabel!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CheckoutBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView

    }
    
    func updateTotal()
    {
        print("total");
    }
}
